package com.app.mercedes.nfc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SummaryActivity extends AppCompatActivity {
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    EditText editNFC, editPHOTO, editPhone;
    Button SaveButton;
    String HttpUrl = "http://kmids.ctt-center.com/api/Visitor";
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_summary );
        StrictMode.setThreadPolicy( new StrictMode.ThreadPolicy.Builder().permitAll().build() );
        requestQueue = Volley.newRequestQueue( SummaryActivity.this );
        editNFC = (EditText) findViewById( R.id.textView );
        editPHOTO = (EditText) findViewById( R.id.textView2 );
        editPhone = (EditText) findViewById( R.id.textView3 );
        SaveButton = (Button) findViewById( R.id.btnSave );
        final SharedPreferences prefs = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE );
        editPHOTO.setText( prefs.getString( "name", "" ) );
        editNFC.setText( prefs.getString( "NFC", "" ) );
        SaveButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jo = new JSONObject();
                JSONObject apiRequest = new JSONObject();
                JSONArray data = new JSONArray();
                JSONObject guardData = new JSONObject();
                try {
                    apiRequest.put( "action", "create" );
                    jo.put( "apiRequest", apiRequest );
                    guardData.put( "imageName", prefs.getString( "name", "None" ) );
                    guardData.put( "cardId", prefs.getString( "NFC", "None" ) );
                    guardData.put( "telephone", editPhone.getText().toString() );
                    guardData.put( "entryDate", getDateTime() );
                    data.put( guardData );
                    jo.put( "data", data );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JsonObjectRequest jsonRequest = new JsonObjectRequest( Request.Method.POST, HttpUrl, jo, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText( SummaryActivity.this, "Check in Complete", Toast.LENGTH_SHORT ).show();
                        Intent vv = new Intent( SummaryActivity.this, MainActivity.class );
                        startActivity( vv );
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                } );
                RequestQueue requestQueue = Volley.newRequestQueue( SummaryActivity.this );
                requestQueue.add( jsonRequest );
            }
        } );
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss", Locale.getDefault() );
        Date date = new Date();
        return dateFormat.format( date );
    }
}