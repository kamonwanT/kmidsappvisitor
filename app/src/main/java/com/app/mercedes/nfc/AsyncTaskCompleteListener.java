package com.app.mercedes.nfc;

public interface AsyncTaskCompleteListener {
    void onTaskCompleted(String response, int serviceCode);
}