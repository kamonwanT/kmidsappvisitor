package com.app.mercedes.nfc;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class HeadActivity extends AppCompatActivity {
    private static final int TIME_OUT = 1500;
    private Handler mHandler = new Handler();
    private String Null = "null";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_head);
        autochangeActivity();
    }

    public void autochangeActivity() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(HeadActivity.this, MainActivity.class));
                finish();
            }
        }, TIME_OUT);

    }
    }
