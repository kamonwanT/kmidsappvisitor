package com.app.mercedes.nfc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class CameraActivity extends AppCompatActivity implements AsyncTaskCompleteListener {
    private ParseContent parseContent;
    private Button vvv;
    private ImageView imageview;
    private static final String IMAGE_DIRECTORY = "/GuardIMG";
    private final int CAMERA = 1;
    private AQuery aQuery;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    EditText ccc;
    RequestQueue requestQueue;
    String HttpUrl = "http://kmids.ctt-center.com/api/Visitor";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_camera );
        parseContent = new ParseContent( this );
        aQuery = new AQuery( this );
        requestQueue = Volley.newRequestQueue( CameraActivity.this );
        Intent intent = new Intent( android.provider.MediaStore.ACTION_IMAGE_CAPTURE );
        startActivityForResult( intent, CAMERA );
        vvv = (Button) findViewById( R.id.button3 );
        ccc = (EditText) findViewById( R.id.editText );
        imageview = (ImageView) findViewById( R.id.imageView );
        final SharedPreferences prefs = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE );
        vvv.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jo = new JSONObject();
                JSONObject apiRequest = new JSONObject();
                JSONArray data = new JSONArray();
                JSONObject guardData = new JSONObject();
                try {
                    apiRequest.put( "action", "create" );
                    jo.put( "apiRequest", apiRequest );
                    guardData.put( "imageName", prefs.getString( "name", "None" ) );
                    guardData.put( "cardId", prefs.getString( "NFC", "None" ) );
                    guardData.put( "telephone", ccc.getText().toString() );
                    guardData.put( "entryDate", getDateTime() );
                    data.put( guardData );
                    jo.put( "data", data );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JsonObjectRequest jsonRequest = new JsonObjectRequest( Request.Method.POST, HttpUrl, jo, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText( CameraActivity.this, "Check in Complete", Toast.LENGTH_SHORT ).show();
                        Intent vv = new Intent( CameraActivity.this, HeadActivity.class );
                        startActivity( vv );
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                } );
                RequestQueue requestQueue = Volley.newRequestQueue( CameraActivity.this );
                requestQueue.add( jsonRequest );
            }
        } );
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss", Locale.getDefault() );
        Date date = new Date();
        return dateFormat.format( date );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get( "data" );
            imageview.setImageBitmap( thumbnail );
            String path = saveImage( thumbnail );
            Toast.makeText (this,path,Toast.LENGTH_LONG).show ();
            try {
                uploadImageToServer( path );
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadImageToServer(final String path) throws IOException, JSONException {
        if (!AndyUtils.isNetworkAvailable( CameraActivity.this )) {
            Toast.makeText( CameraActivity.this, "Internet is required!", Toast.LENGTH_SHORT ).show();
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put( "url", "http://kmids.ctt-center.com/api/upload" );
        map.put( "filename", path );
        new MultiPartRequester( this, map, CAMERA, this );
        AndyUtils.showSimpleProgressDialog( this );
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        AndyUtils.removeSimpleProgressDialog();
//        SharedPreferences.Editor editor = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE ).edit();
//        editor.putString( "name", response );
//        editor.apply();
        Toast.makeText( this, "Upload Image Successfully", Toast.LENGTH_LONG ).show();
        Log.d( "res", response );
        switch (serviceCode) {
            case CAMERA:
                if (parseContent.isSuccess( response )) {
                    String url = parseContent.getURL( response );
                    aQuery.id( imageview ).image( url );
                }
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress( Bitmap.CompressFormat.JPEG, 100, bytes );
        File wallpaperDirectory = new File( Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY );
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        try {
            SharedPreferences settings = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE );
            String cardId = settings.getString ("NFC","");

            File f = new File( wallpaperDirectory, cardId + ".jpg" );
            f.createNewFile();

            FileOutputStream fo = new FileOutputStream( f );
            fo.write( bytes.toByteArray() );
            MediaScannerConnection.scanFile( this, new String[]{f.getPath()}, new String[]{"image/jpeg"}, null );
            fo.close();
            Log.d( "TAG", "File Saved::--->" + f.getAbsolutePath() );
            //SharedPreferences.Editor editor = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE ).edit();
            //editor.putString( "name", f.getAbsolutePath() );
            //editor.apply();
            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }


}