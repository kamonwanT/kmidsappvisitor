package com.app.mercedes.nfc;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ShowOutActivity extends AppCompatActivity {

    private static final String IMAGE_DIRECTORY = "/GuardIMG";
    Button btnCHout;
    private ImageView IMGView;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String HttpUrl = "http://kmids.ctt-center.com/api/Visitor";
    RequestQueue requestQueue;
    private static Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_show_out );
        requestQueue = Volley.newRequestQueue( ShowOutActivity.this );
        final SharedPreferences prefs = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE );
        btnCHout = (Button) findViewById( R.id.button3 );
        IMGView = (ImageView) findViewById( R.id.imageView );
        //final String[] strPath = {prefs.getString( "name", "None" )};
        String imageDirectory =  Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY ;

        SharedPreferences settings = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE );
        String cardId = settings.getString ("NFC","");
        final Bitmap bm = BitmapFactory.decodeFile( imageDirectory + "/" + cardId + ".jpg" );
        IMGView.setImageBitmap( bm );

                btnCHout.setOnClickListener (new OnClickListener () {
                    @Override
                    public void onClick (View v) {
                        JSONObject jo = new JSONObject ();
                        JSONObject apiRequest = new JSONObject ();
                        JSONArray data = new JSONArray ();
                        JSONObject guardData = new JSONObject ();
                        try {
                            apiRequest.put ("action", "get");
                            jo.put ("apiRequest", apiRequest);
                            guardData.put ("cardId", prefs.getString ("NFC", "None"));
                            guardData.put ("exitDate", getDateTime ());
                            data.put (guardData);
                            jo.put ("data", data);
                        } catch (JSONException e) {
                            e.printStackTrace ();
                        }
                        JsonObjectRequest jsonRequest = new JsonObjectRequest (Request.Method.POST, HttpUrl, jo, new Response.Listener<JSONObject> () {
                            @Override
                            public void onResponse (JSONObject response) {
                                String imageDirectory =  Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY ;
                                SharedPreferences settings = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE );
                                String cardId = settings.getString ("NFC","");
                                Log.d("Checkout",cardId);

                                File f = new File( imageDirectory, cardId + ".jpg" );
                                f.delete ();
                                MediaScannerConnection.scanFile( ShowOutActivity.this, new String[]{imageDirectory + "/" + cardId + ".jpg"},null, null );
                                Toast.makeText (ShowOutActivity.this,"checkoutsuccess",Toast.LENGTH_LONG).show ();
                                Intent vv = new Intent (ShowOutActivity.this, HeadActivity.class);
                                startActivity (vv);
                            }
                        }, new Response.ErrorListener () {
                            @Override
                            public void onErrorResponse (VolleyError error) {
                            }
                        });
                        RequestQueue requestQueue = Volley.newRequestQueue (ShowOutActivity.this);
                        requestQueue.add (jsonRequest);
                    }
                });


    }
    private void deleteImage()
    {
        File wallpaperDirectory = new File( Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY );

    }

    private String getDateTime () {
                SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.getDefault ());
                Date date = new Date ();
                return dateFormat.format (date);

    }
    }
